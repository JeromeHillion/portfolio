<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/features.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <title>Document</title>
</head>

<body class="bg-light">
    <div class="container">

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark rounded-top mt-4">
            <div class="container-fluid">
                <div class="border border-info border-2 rounded p-3 text-center ms-3">
                    <a class="text-info fs-3" href="#">JH</a>
                </div>


                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse row justify-content-md-center" id="navbarColor01">
                    <ul class="navbar-nav col-md-auto  mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">à propos de moi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">experiences professionnelles</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Formation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">compétences techniques</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">soft skills</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="row">
            <img class="img-fluid" src="http://via.placeholder.com/1320x456">
        </div>


        <div class="text-center border bg-white p-3">
            <div class="row justify-content-md-center">
                <div class="col-md-auto">
                    <div class="text-center">
                        <img src="https://fakeimg.pl/150x150/" class="rounded-circle" alt="...">
                        <h5 class=" card-title text-center border-bottom border-3 border-info">à propos de moi</h5>
                    </div>
                </div>
                <div class="col-md-auto p-3">
                    <div class="text-center">
                        <p class="card-text">Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                            Nostrum ullam rem repellat nam quod quia nobis, odit, minima consectetur ipsum
                            earum aliquid natus neque veniam tempora soluta ducimus fugiat repudiandae!
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis nam pariatur enim eveniet dolore tenetur quae, vitae amet minima ducimus officia illum ullam molestiae itaque molestias porro atque veritatis rerum.</p>
                        <button type="button" class="btn btn-outline-info">Télécharger mon CV</button>
                        <button type="button" class="btn btn-outline-warning">Contacter moi</button>
                    </div>
                </div>
            </div>
        </div>


        <main class="container p-0">
            <div class="d-flex align-items-center p-3 my-3 text-white bg-purple rounded shadow-sm bg-info">
                <img class="me-3" src="images/ordinateur.svg" alt="" width="48" height="38">
                <div class="lh-1">
                    <h1 class="h6 mb-0 text-white lh-1">Experiences professionnelles</h1>
                </div>
            </div>

            <div class="my-3 p-3 bg-body rounded shadow-sm">
                <div class="d-flex text-muted pt-3">
                    <img class="border p-2 me-2 rounded" width="64" height="64" src="images/avicom.jpg" role="img" aria-label="Placeholder: Avicom'" preserveAspectRatio="xMidYMid slice" focusable="false">
                    <rect width="100%" height="100%" fill="#007bff" /><text x="50%" y="50%" fill="#007bff" dy=".3em" class="text-info fw-bold">Avicom'</text></img>

                    <p class="pb-3 mb-0 small lh-sm">
                        <strong class="d-block text-gray-dark">Développeur PHP SYMFONY</strong>
                        <span class="d-block text-dark">09/2020 - 11/2020</span>
                        Développement d'application web pour les clients de l'agence, les principaux projets
                        se faisaient avec Symfony et Wordpress, ou tout simplement avec les langages de
                        développement web seul (html, css, javascript, PHP).
                    </p>
                </div>
            </div>
            <div class="my-3 p-3 bg-body rounded shadow-sm">
                <div class="d-flex text-muted pt-3">
                    <img class="border p-2 me-2 rounded" width="64" height="64" src="images/caisse-epargne-logo.jpg" role="img" aria-label="Placeholder: Avicom'" preserveAspectRatio="xMidYMid slice" focusable="false">
                    <rect width="100%" height="100%" fill="#007bff" /><text x="50%" y="50%" fill="#007bff" dy=".3em" class="text-info fw-bold">CAISSE D'EPARGNE</text></img>

                    <p class="pb-3 mb-0 small lh-sm">
                        <strong class="d-block text-gray-dark">Intégrateur WEB</strong>
                        <span class="d-block text-dark">08/2018 - 09/2018</span>
                        Participation aux activités récurrentes, notamment produire et mettre en ligne des
                        contenus sur les déférents supports internes/externes (sites institutionnel, intranet,
                        newsletter...). La mise en ligne des articles et des publications à l'aide des outils mis
                        à disposition et optimiser leur présentation. Garantir de la qualité et à la fiabilité des
                        documents mis en ligne (gabarit, charte graphiques).
                </div>
            </div>
            <div class="my-3 p-3 bg-body rounded shadow-sm">
                <div class="d-flex text-muted pt-3">
                    <img class="border p-2 me-2 rounded" width="64" height="64" src="images/Gîtes_de_France_(logo).svg.png" role="img" aria-label="Placeholder: Avicom'" preserveAspectRatio="xMidYMid slice" focusable="false">
                    <rect width="100%" height="100%" fill="#007bff" /><text x="50%" y="50%" fill="#007bff" dy=".3em" class="text-info fw-bold">SAS GITES DE FRANCE</text></img>

                    <p class="pb-3 mb-0 small lh-sm">
                        <strong class="d-block text-gray-dark">Développeur PHP</strong>
                        <span class="d-block text-dark">09/2020 - 11/2020</span>
                        Traduction des besoins graphiques et techniques Mise en place
                        de normes hôtelières (schémas XSD) avec sa documentation (définition des tables
                        et valeurs) Création d’un parseur XML en PHP et d’une plateforme d’envoi de mails
                        avec pièces jointes Gestion de serveurs et de bases de données
                    </p>
                </div>
            </div>
            <div class="d-flex align-items-center p-3 my-3 text-white bg-purple rounded shadow-sm bg-warning">
                <img class="me-3" src="images/school.svg" alt="" width="48" height="38">
                <div class="lh-1">
                    <h1 class="h6 mb-0 text-white lh-1">Formation</h1>
                </div>
            </div>
            <div class="container p-3 rounded bg-white shadow-sm" id="featured-3">
                <div class="row g-4 py-4 row-cols-1 row-cols-lg-3">
                    <div class="feature col ">
                        <div class="row justify-content-md-center">
                            <div class="col-md-auto border  p-2 rounded-3 ">
                                <img src="images/3wa.png" width="50" height="50">
                            </div>
                        </div>
                        <h4 class="text-warning text-center ">3W ACADEMY</h4>
                        <p class="text-secondary text-center">Paragraph of text beneath the heading to explain the heading. We'll add onto it with another sentence and probably just keep going until we run out of words.</p>
                        <div class="d-grid gap-2 col-6 mx-auto">
                            <button type="button" class="btn btn-outline-success btn-sm">Voir le détail</button>
                        </div>
                    </div>

                    <div class="feature col">
                        <div class="row justify-content-md-center">
                            <div class="col-md-auto border  p-2 rounded-3 ">
                                <img src="images/Greta.jpg" width="50" height="50">
                            </div>
                        </div>
                        <h4 class="text-warning text-center">GRETA 94 LYCEE CHAMPLAIN</h4>
                        <p class="text-secondary text-center">Paragraph of text beneath the heading to explain the heading. We'll add onto it with another sentence and probably just keep going until we run out of words.</p>
                        <div class="d-grid gap-2 col-6 mx-auto">
                            <button type="button" class="btn btn-outline-success btn-sm">Voir le détail</button>
                        </div>
                    </div>
                    <div class="feature col ">
                        <div class="row justify-content-md-center">
                            <div class="col-md-auto border  p-2 rounded-3 ">
                                <img src="images/Afpa.png" width="50" height="50">
                            </div>
                        </div>
                        <h4 class="text-warning text-center">AFPA 77 CHAMPS SUR MARNE</h4>
                        <p class="text-secondary text-center">Paragraph of text beneath the heading to explain the heading. We'll add onto it with another sentence and probably just keep going until we run out of words.</p>
                        <div class="d-grid gap-2 col-6 mx-auto">
                            <button type="button" class="btn btn-outline-success btn-sm">Voir le détail</button>
                        </div>
                    </div>

                    <div class="feature col ">
                        <div class="row justify-content-md-center">
                            <div class="col-md-auto border  p-2 rounded-3 ">
                                <img src="images/logo-Web-Force-3-Les-Mureaux.png" width="50" height="50">
                            </div>
                        </div>
                        <h4 class="text-warning text-center">WEBFORCE3 NANTERRE</h4>
                        <p class="text-secondary text-center">Paragraph of text beneath the heading to explain the heading. We'll add onto it with another sentence and probably just keep going until we run out of words.</p>
                        <div class="d-grid gap-2 col-6 mx-auto">
                            <button type="button" class="btn btn-outline-success btn-sm">Voir le détail</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-flex align-items-center p-3 my-3 text-white bg-purple rounded shadow-sm bg-success">
                <img class="me-3" src="images/personal.svg" alt="" width="48" height="38">
                <div class="lh-1">
                    <h1 class="h6 mb-0 text-white lh-1">Compétences techniques</h1>
                </div>
            </div>
            <div class="container px-4 py-3 bg-white" id="featured-3">
                <h2 class="pb-2 border-bottom">Front-End</h2>
                <div class="g-4 py-3">
                    
                        <div class="row row-cols-auto justify-content-around">
                            <div class="col border p-2 rounded-3 ">
                                <img src="images/1024px-HTML5_logo_and_wordmark.svg.png" width="50" height="50">
                            </div>

                            <div class="col border p-2 rounded-3 ">
                                <img src="images/css3.png" width="50" height="50">
                            </div>

                            <div class="col border p-2 rounded-3 ">
                                <img src="images/js.png" width="50" height="50">
                            </div>

                            <div class="col border p-2 rounded-3 ">
                                <img src="images/Bootstrap_logo.svg.png" width="50" height="50">
                            </div>

                            <div class="col border p-2 rounded-3 ">
                                <img src="images/webpackencore.png" width="50" height="50">
                            </div>

                            <div class="col border p-2 rounded-3 ">
                                <img src="images/Twig.jpg" width="50" height="50">
                            </div>
                        </div>
                    


        </main>
    </div>
</body>

</html>